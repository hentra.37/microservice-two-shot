from django.db import models

# Create your models h ere.
class LocationVO(models.Model):
    import_href = models.CharField(max_length=120, unique=True)
    closet_name = models.CharField(max_length=120)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return f"{self.closet_name} - {self.section_number}/{self.shelf_number}"


class Hat(models.Model):
    fabric = models.CharField(max_length=120)
    style = models.CharField(max_length=120)
    color = models.CharField(max_length=120)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE
    )
